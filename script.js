const loaderContainer = document.createElement('div')

const createLoader = () => {
    loaderContainer.id = 'loader-container'

    const loaderWrapper = document.createElement('div')

    loaderWrapper.classList.add('loader-wrapper')

    const bar1 = document.createElement('div')
    const bar2 = document.createElement('div')
    const bar3 = document.createElement('div')

    bar1.classList.add('bar')
    bar2.classList.add('bar')
    bar3.classList.add('bar')

    loaderWrapper.appendChild(bar1)
    loaderWrapper.appendChild(bar2)
    loaderWrapper.appendChild(bar3)
    loaderContainer.appendChild(loaderWrapper)
    document.body.appendChild(loaderContainer);
}

const showLoading = () => {
    document.body.classList.add('hide-scroll')
    loaderContainer.classList.add('visible')
}

const hideLoading = () => {
    document.body.classList.remove('hide-scroll')
    loaderContainer.classList.remove('visible')
}

const download = () => {
    showLoading()

    const tags = document.getElementsByTagName('script');

    for (let i = 0; i < tags.length; i++) {
        const tag = tags.item(i);

        if (!tag.text.includes('imagens_cap')) {
            continue;
        }

        const { innerText } = tag;
        const normalizedText = innerText.trim().replace('var imagens_cap = ', '').replace(';', '');
        const imagesList = JSON.parse(normalizedText);

        const titleElement = document.getElementById('manga-title')
        const title = titleElement.innerText.replace('Ler ', '').replace(/ /g, '-').toLowerCase()

        const body = {
            name: title,
            images: imagesList
        }

        window.fetch('https://muitomanga-pdf-downloader.herokuapp.com/convert/pdf', {
            method: 'POST',
            mode: 'cors',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        }).then(res => {
            const header = res.headers.get('Content-disposition')
            const namePosition = header.indexOf('filename')

            return {
                filename: header.slice(namePosition).replace('filename=', ''),
                blobPromise: res.blob()
            }
        }).then(res => {
            res.blobPromise.then((blob) => {
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(blob);
                } else {
                    const objUrl = window.URL.createObjectURL(blob);

                    let link = document.createElement('a');
                    link.href = objUrl;
                    link.download = res.filename;
                    link.click();
                    link.remove();

                    setTimeout(() => { window.URL.revokeObjectURL(objUrl) }, 250);
                }

                hideLoading()
            }).catch(() => hideLoading())
        }).catch(err => {
            hideLoading()
            console.log(err)
        })
    }
}

const insertButton = () => {
    const elements = document.getElementsByClassName('widget_senction2')

    let container = document.getElementsByClassName('widget_senction2')[0]

    for (const element of elements) {
        if (element.dataset.idCap) {
            container = element
        }
    }

    const child = container.children[0]
    const title = child.children.item(0)

    title.id = 'manga-title'

    child.classList.add('container-adjust')

    const button = document.createElement('button')
    button.classList.add('pdf-download-button')
    button.innerText = 'Baixar em PDF'
    button.addEventListener('click', download)

    child.appendChild(button)
}

createLoader();
insertButton();